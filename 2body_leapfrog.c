    #include <stdio.h>
    #include <math.h>
    #include <stdlib.h>
    
    int main() {
            double r[3], v[3], a[3];                
            double dt;                              
            int i, k;
            FILE *fp;
            
            dt = 1e-5;
    
            r[0] = 1;
            r[1] = 0;
            r[2] = 0;
            v[0] = 0;
            v[1] = 0.5;
            v[2] = 0;
            
            fp = fopen("data4.txt", "w+");
            for (i = 0; i < 100000; i++) {
                    double r2 = r[0]*r[0] + r[1]*r[1] + r[2]*r[2]; 
                    for (int k = 0; k < 3; k++) {
                            a[k] = - r[k] / (r2 * sqrt(r2));        
                            v[k] += 0.5 * a[k] * dt;                
                            r[k] += v[k] * dt;                      
                    }
            fprintf(fp, "%f %f \n", r[0], r[1]);
            }
            flcose(fp);
            
            printf(" \n   r[0]=%10.8f \n", r[0]);
            printf(" \n   r[1]=%10.8f \n", r[1]);
            printf(" \n   r[2]=%10.8f \n", r[2]);
            printf(" \n   v[0]=%10.8f \n", v[0]);
            printf(" \n   v[1]=%10.8f \n", v[1]);
            printf(" \n   v[2]=%10.8f \n", v[2]);

            return 0;
    }
