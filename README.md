This is the readme file that will track my progress through the final project of computer programming course IND3156.

# Information about n-body simulations

Link: http://physics.princeton.edu/~fpretori/Nbody/intro.htm

An N-body simulation approximates the motion of particles, often specifically particles that interact with one another through some type of physical forces. Applications range from the orbits and movement of stars, planet, asteroids, to individual atoms in a gas cloud.

## Focus on gravitational interactions

The most obvious way to perform this simulation is through direct integration of the Newtonian gravitational force equation, where the total force on each particle is the superposition of the forces imparted by each of the other particles. 
	Image equation
By calculating the net force on each particle, the new velocity and position can be calculated using a discretized time step, dt [brute force method]. The only approximation is the constant acceleration of particles during the time dt.
The problem with this algorithm is large computational time since it scale as N² where N is number of particles. If the number of particles doubles, the computational time quadruples, thereby making it hard to compute large N. See section of Barnes-Hut Algorithm.

A sample N-body simulation program taken from the Effective Computation in Physics textbook was used to gain insight into the general structure of such a code. It is written in Python. Playing with it, means that various version now exist in the repository. In parallel, I worked on developing alongside Moving Around Stars text a gradual approach to building an n-body problem. Starting with 2 bodies, moving to 3 and then up.

## The Gravitational 2-body problem
--
Starting with the simplest case that is non trivial, 2 bodies. Although this is a solved calculation, done by Newton, we can apply the general framework of orbit formation from a manual formulaic form to a computer program. 

We simplify the N=2 system further by having relative positions and velocities rather than absolute ones. This is interesting statement from the author since at the end of the day, all motion and position is relative and requires a reference frame. However, it is true that for a computer we are bounding the system and thus have to allow the bodies to have absolute positions. 

The relative position of the two particles obtained from r1and r2is rcoming from,

    r = r_2 - r_1

Newton’s gravitational equation of motion then becomes:

    d^2/dt^2 r = -G M_1+M_2/r^3 r

A second order differential equation. The second derivative of position with respect to time on the left is the acceleration. 

We are working in three dimensions and thus ris a vector composed of,

    r = {x,y,z}

And the scalar distance is found to be,

    r = |r| = x^2+y^2+z^2

In the following model we will then be exploiting the simplified fact that we will maintain that,

    G=1
    M_1+M_2=1

And the motion is thus,

    d^2/dt^2 r = r/r^3

### Exploring N=2 with a Forward-Euler Algorithm

Each time step in the first-order integrator is a tangential to the orbit we are on. After that, at the next step, the new value of the acceleration changes the direction and motion. We then construct an approximate orbit of many small tangents. 

    r_(i+1) = r_i+v_i dt
    v_(i+1) = v_i +a_i dt

Position and velocity of an individual particle where i indicates the values for time t_i and i+1 for the time t_(i+1) after one time step has been taken, dt = t_(i+1)-t_i. The acceleration on a particle by the gravitational forces of all the other particles summed up is a.

Pseudo code of 2body_euler.c,

    int main() {
        Define variables
        double 
        int
        arrays for location, velocity, acceleration
        
        Assign a timestep for dt
        dt = 
        
        Define starting values for the elements in the arrays of position and velocity. these are abitrary
        r[0] = first dimension of location, i.e. x
        r[1] = second dimension of location, i.e. y
        so on and the same for velocity.
        
        Open the txt file for the data
        for loop. Number of loops is important, the more there are, the smaller the error and the closer to actual orbit the program comes to.
            the distance between the two bodies, r2, is found for subsequent acceleration operations
            for loop. Limited to the array of elements
                a[k] = array for acceleration
                r[k] = array for location
                v[k] = array for velocity
        
        print function outputs in txt file as they go
        
        close txt file
        
    return 0; }

Visualizing text on a graph in Octave,

    define data as the contents of the text file
    plot the data on a two dimensional graph



### Leapfrog Algorithm at N=2

This is a second order integrator that has positions and velocities ‘leap over’ each other. This is not great for accuracy but beneficial towards building a prototype that leads us forward to a larger N. Later a higher order integrator will replace the leap frog. The leaping follows, time as ti, ti+1, ti+2spaced at constant intervals dtwhile the velocity is at times halfway in between as dt/2 and ti-i/2, ti+1/2, ti+3/2. The leapfrog integration is position, and velocity thus, 

    r_i = r_i-1 + v_(i-1/2) dt
    v_(i+1/2) = v_(i-1/2) + a_i dt

The acceleration is defined as prior while the velocities are defined only on half-integer times. This makes sense, given that a(r, v) = a(r): the acceleration on one particle depends only on its position with respect to all other particles, and not on its or their velocities. Only at the beginning of the integration do we have to set up the velocity at its first half-integer time step. Starting with initial conditions r_0 and v_0, we take the first term in the Taylor series expansion to compute the first leap value for v, v_(1/2) = v_0 + a_0 dt/2
We can then apply the operations mentioned earlier.


## Graduating Leapfrog to N=3

The expression for acceleration felt by particle iis the sum of the Newtonian gravitational attraction of all other particles j, where both i and j take on values from 1 up to including N,

    (d^2 / dt^2) * r_i = G*(sum of M_j*[(r_j-r_i) / (|r_j-r_i|^3)] until N for j = 1 and j != i)

Here M_j and r_j are the mass and position of vector of particle j, and G is the gravitationaé constant. We can the get the acceleration as,

    a_i= G(sum of [M_j / (r_ji^2)]*r_ji

The code will thus not be as simple as that of a two body since we cannot reduce the acceleration as easily nor just work with one position r, we have to keep track of all three velocities, locations, and accelerations. This means the code is quite lengthier and more complex. I have not had the time to write up a full pseudo code. Here's a rough one,

## Improving runtime using the Barnes-Hut algorithm

[The following sections of the project were not actually worked through thoroughly and were abandoned when I realised that the Barnes-Hut algorithm is just a component of a much larger N-body algorithm, the latter which I did not understand fully (in code) yet.]

As we build more and more complex programs to simulate the movement of N bodies orbiting /interacting with each other, the computational time increases drastically. This makes it impractical when working our way up to large N. Therefore, to decrease the number of force calculations per time step, we can disregard individual bodies that are far away from the body whose net force we are trying to calculate. Since the gravitational force decreases as 1/r², bodies become negligible quite rapidly. The Barnes-Hut algorithm provides a systematic way to define "far-away" along with providing a method for approximating the force due to far-away bodies. 

An quad-tree data structure is used to represent the position of the bodies in the defined space. The finite region of space is divided into quadrants, each of which will hold a number of bodies. Each of these subspaces is assigned a mass, which is defined to be located at the center of mass of the bodies in that quadrant with a mass equal to their sum. Then, each quadrant that contains more than one body is again subdivided, this continues recursively until each quadrant contains either zero or one bodies. 

In-depth look at the Barnes-Hut Algorithm
--
To speed up the brute force n-body algorithm, the following method groups nearby bodies and approximate them as a single body. If the group is sufficiently far away, we can approximate its gravitational effects by using its center of mass. The center of mass of a group of bodies is the average position of a body in that group, weighted by mass. Formally, if two bodies have positions (x1, y1) and (x2, y2), and masses m1 and m2, then their total mass and center of mass (x, y) are given by,
    
    m = m1 + m2
    x = (x1*m1 + x2*m2) / m
    y = (y1*m1 + y2*m2) / m

The Barnes-Hut algorithm is a clever scheme for grouping together bodies that are sufficiently nearby. It recursively divides the set of bodies into groups by storing them in a quad-tree. Each external node represents a single body. Each internal node represents the group of bodies beneath it, and stores the center-of-mass and the total mass of all its children bodies. Image To calculate the net force on a particular body, traverse the nodes of the tree, starting from the root. If the center-of-mass of an internal node is sufficiently far from the body, approximate the bodies contained in that part of the tree as a single body, whose position is the group’s center of mass and whose mass is the group’s total mass.

If the internal node is not sufficiently far from the body, recursively traverse each of its subtrees. To determine if a node is sufficiently far away, compute the quotient s / d, where s is the width of the region represented by the internal node, and d is the distance between the body and the node’s center-of-mass. If s / d < θ, then the internal node is sufficiently far away.The θ parameter affects the speed and accuracy of the simulation. Common practice is to use θ as 0.5 and when using it as 0 means treating everything as a singular body.

Method retrieved from (http://arborjs.org/docs/barnes-hut)
1. If node x does not contain a body, put the new body b here.
2. If node x is an internal node, update the center-of-mass and total mass of x. Recursively insert the body b in the appropriate quadrant.
3. If node x is an external node, say containing a body named c, then there are two bodies b and c in the same region. Subdivide the region further by creating four children. Then, recursively insert both b and c into the appropriate quadrant(s). Since b and c may still end up in the same quadrant, there may be several subdivisions during a single insertion. Finally, update the center-of-mass and total mass of x.

# Building and understanding quadtree structure.

The quadtree is a a tree data structure in which each internal node has exactly four “children”. The most common use, that is the one we’re interested of, is to partition a two dimensional space by recursively subdividing it into four quadrants.  All forms of quadrees usually share some common features,
Deconstructs space into adaptable cells.
Each cell has a maximum capacity. For the project, only one body.
The tree directory follows the spatial decomposition of the quadtree.

Pseudo code for a quadtree in c inspired from Wikipedia. (link: https://en.wikipedia.org/wiki/Quadtree)

Structures and space.

    class definitions {
        
    define x;
    define y;

    import data
    }

Main

    class main QuadTree
    {
    int node_capacity = 1;                                          // Arbitrary constant of how many elements can be stored in this quadtree node.  For us this will be limited to 0 or 1. Not more.
      
    AABB boundary;                                                  // Space bounds

    Array of XY [size = node_capacity] points;                      // Array of elements
                                                                    // Children - nodes that branch out - split the bodies into new subspaces.
    QuadTree* northWest;                                            // I will call this A                      
    QuadTree* northEast;                                            // I will call this B
    QuadTree* southWest;                                            // I will call this C
    QuadTree* southEast;                                            // I will call this D
                                                                    // subroutines calls
    function construct(AABB _boundary)                              // function that will create box
    function insert()                                               // function that will place the points created into quadrants
    function subdivide()                                            // function that create four children that fully divide this quad into four quads of equal area
    }

Inserting bodies in the correct subspace / making new nodes if necessary.

    function insert()
    {
     
    if (number of points < node capacity)                            //if function that checks if there is space in the quadtree to add an elements
    {
      append poinds;
      return true;
    }

    if (A is not empty)                                              //otherwise, subdivide and then add the point to whichever node will accept it
        subdivide();

    if (A then insert(p)) return true;                          
    if (B then insert(p)) return true;
    if (C then insert(p)) return true;
    if (D then insert(p)) return true;

    return false;
      }
    }


# Calculating the force acting on a body

Calculating the net force acting on body b we employ the following methodology retrieved from (http://arborjs.org/docs/barnes-hut)
1. If the current node is an external node (and it is not body b), calculate the force exerted by the current node on b, and add this amount to b’s net force.
2. Otherwise, calculate the ratio s/d. If s/d < θ, treat this internal node as a single body, and calculate the force it exerts on body b, and add this amount to b’s net force.
3. Otherwise, run the procedure recursively on each of the current node’s children.
