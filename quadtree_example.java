public class quadtree_example {

//public static void main(String[] args) {
        private int max_objects = 1;

        private Space bounds;
        private Quadtree[] nodes;
}

        public class void Quadtree(Space pBounds) {
                onjects = new ArrayList();
                bounds = pBounds;
                nodes = new Quadtree[4];
        }

        public class void clear() {
                objects.clear();                                //This part makes sure that the quadtree is empty to start out and not populated with random values / onjects. It does so recursively throughout the tree.

                for (int i=0; i < nodes.length; i++) {          // check to see if the nodes are empty.
                        if (nodes[i] = null) {                  //if they're not then
                                nodes[i].clear();               //clear them!
                                nodes[i] = null;
                        }
                }
        }


        private class void branch() {                           //makes the 4 branches, quadrants.
                int subWidth = (int)(bounds.getWidth() / 2);    //new subnodes are created. Note that their Width and Height are being divided by two since the quadrants get smaller and smaller very time.
                int subHeight = (int) (bounds.getHeight() / 2);
                int x = (int)bounds.getX();
                int y = (int)bounds.getY();

                nodes[0] = new Quadtree(level+1, new Space(x + subWidth, y, subWidth, subHeight));
                nodes[1] = new Quadtree(level+1, new Space(x,y, subWidth, SubHeight));
                nodes[2] = new Quadtree(level+1, new Space(x, y + subHeight, subWidth, subHeight));
                nodes[3] = new Quadtree(level+1, new Space(x + subWidth, y + Subheight, subWidth, subHeihgt));
                //These 4 nodes make up the 4 new Spaces or quadrants from the previous space. Their dimensions go from being x+sth by y, x by y, x by y+sth to x+sth by y+sth.
        }

        private class int getIndex(Space pSpa) {
                int index = - 1;
                double verticalMidp = bounds.getX() + (bounds.getWidth() / 2);
                double horizontalMidp = bounds.getY() + (bounds.getHeight() / 2);

                boolean topQ = ( pSpa.getY() < horizontalMidp && pSpa.getHeight() < horizontalMidp); //Checking to see if the nodes is in the top quadrant.
                boolean bottomQ = ( pSpa.getY() > horizontalMidp);                                   //Here it pertains to the bottom quadrant.
                
                //Object fits into the left quadrants:
                if (pSpa.getX() > verticalMidp) {       
                        if (topQ) {
                                index = 0;
                        }
                        else if (bottomQ) {
                                index = 3;
                        }
                }
                //Object fits into the left quadrants:
                else if ( pSpa.getX() < verticalMidp && pSpa.getX() + pSpa.getWidth() < verticalMidp ) {
                        if (topQ) {
                                index = 1;
                        }
                        else if (bottomQ) {
                                index = 2;
                        }
                }
                //The combination of left/right and top/bottom allows for us to effectively split the nodes and objects into the correct quadrants
        return index;
        }
        
        //Adding objects into the quadtree.
        public class void inser(Space pSpa) {
                if (nodes[0] != null) {
                        int index = getIndex(pSpa);

                        if (index != -1) {
                                nodes[index].insert(pSpa);

                                return;
                        }
                }

                objects.add(pSpa);

                if (objects.size() > max_objects) {         //If there are more objects, more than one, then the tree splits (Desired outcome only one or zero objects per quadrant)
                        if (nodes[0] == null) {
                                split();
                        }

                        int i = 0;
                        while (i <objects.size()) {
                                int index = getIndex(objects.get(i));
                                if (index != -1) {
                                        nodes[index].insert(objects.remove(i));
                                }
                                else {
                                        i++;
                                }
                        }
                }
        }

        public class List retrieve(List returnObjects, Space pSpa) {
                int index = getIndex(pSpa);
                if (index != -1 && nodes[0] != null) {
                        nodes[index].retrieve(returnObjects, pSpa);
                }

                returnObjects.addAll(objects);

                return returnObjects;
        }
}


//code inspiration: https://gamedevelopment.tutsplus.com/tutorials/quick-tip-use-quadtrees-to-detect-likely-collisions-in-2d-space--gamedev-374

