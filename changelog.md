## 2body_euler.c

I have a separate changelog with all the information pertaining to 2body_euler.c and 2body_leapfrog.c
The files named 2body_euler.c and 2body_leapfrog.c are codes inspired from Moving Stars Around - The Art of Computational Science (www.artcompsci.org/) where they had a general framework in C++ for what should be done as well as steps to build your own.
The goals is to build the program to a point were it can be transformed to a three body problem and then an N-body problem.
This is the main part of the project. The rest are offshoots that did not work out and led me to narrowing down on the very basics of the problem.

## 3body_leapfrog.c

This is the evolution of the 2body version into 3 bodies. I have not had the time to fully comment and make a pseudo code for it. I also have not implemented the data.txt output from it yet.

## nbody_textbook.python

File named nbody_textbook.python is mostly code from the textbook Effective Computation in Physics.
Code covers the broad and simplistic overview of the nbody simulation. I am however having trouble executing the file since it runs for quite some time but does not produce results to the screen.

Code now works, printed outputs of the runtime of each simulation according to the number of bodies entered.

    Trial with Ns = [2, 4, 8, 16, 32, 64, 128, 256]

Output,

    $ python nbody_textbook.python
    [0.05720400810241699, 0.10225105285644531, 0.20650005340576172, 0.4182090759277344, 0.5228581428527832, 1.0317869186401367, 1.843574047088623, 4.889803171157837]


## nbody.c

File named nbody.c is a translation of nbody_textbook.python simulation into c. This is in progress. A lot of the syntax and manoeuvres done in python are still somewhat unclear and need to be understood. Additionally, I need to make sure that the pointers officiate and work in the correct directions. No executable file has been even attempted due to incompleteness.


## quadtree_example.java

File named quadtree_example.java is mostly code inspired from a website https://gamedevelopment.tutsplus.com/tutorials/quick-tip-use-quadtrees-to-detect-likely-collisions-in-2d-space--gamedev-374 

It has been briefly accomodated to be suited as a foundation for a Barnes-Hut algorithm. Changes include making the maximum number of objects (for us bodies) per node as 1 and keeping the depth of the tree undefined.
It produces a bunch of errors right now due to the public class and return types. Needs to be amended to correct returns and syntax structure for the program to be executable.
This is a stepping stone toewards building my own quadtree program. All of this program is heavily annotated and commented on in order for me to be able to replicate this program in either java, but writing it from scratch with my own syntax, or another language, i.e. c, python, (or fortran).

