    #include <stdio.h>
    #include <math.h>
    #include <stdlib.h>

    int main() {
            int N;
            N = 3;
            double r[N][3], v[N][3], a[N][3];               //3 elements per array per body. since there are 3 bodies, we say N
            double m, pi, phi, t, dt;
            double rji[3], r2, r3
            double absv;                                    //absolute value of velocity
            double end_t;                                   //total end time for the loop to run
            int h, i, j, k;                                 //some of these variables are often reused i.e. i and k, this is fine since we are always limiting their assignment to within for loops and then closing the for loop before reusing i or k and so on.

            m = 1;                                          //mass is 1 and the same for all three bodies
            dt = 1e-3;                                      //timestep
            end_t= 10;                                      //total time durign which we will loop. i.e. here 10/1e-3 = 1000 loops

            pi = 2 * asin(1);                               //pi is defined by the arcsin of 1 which gives pi/2 thus multiplied by 2 to get pi
            for (i = 0; i < N; i++){                        //looping to make sure each element in the array of bodies is assigned the starting locations
                phi = i * 2 * pi / 3;
                r[i][0] = cos (phi);                        //the value of the position k within r[i][k] is chosen according to the following to start out with a cricular orbit
                r[i][1] = sin (phi);
                r[i][2] = 0;
            }

            for (t = 0; t < end_t; t =+dt) {                //we start out with an acceleration of 0.0 and then this will change over time. According to the conditions of time (t), number of bodies (N), and dimensions (h)
                for (i = 0; i < N; i++) {                   //again this is propagated across the array through to all the elements for the bodies
                    for (k = 0; k < 3; k++) {
                        a[i][k] = 0.0;                      //assigning elements of the array an acceleration of 0.0
                        }
                }
            for (i = 0; i < N; i++) {                       //this for loop will be repeated 
                for (j = i+1; j < N; j++){
                    rji[3];                                 //defining the inverse square of gravity, r_ji = r_j -r_i
                    for (k = 0; k < 3; k++) {               //do this for loop for all the elements for the position array
                        rji[k] = r[j][k] - r[i][k];
                        }
                    r2 = 0;
                    for(k = 0; k < 3; k++) {
                        r2 += rji[k] * rji[k];              //this is the r^2 calculation propagated through the array of positions size of k = 3, i.e [0], [1], [2]
                    }
                    r3 = r2 * sqrt(r2);
                    for (k = 0; k < 3; k++) {
                        a[i][k] += m * rji[k] / r3;
                        a[j][k] -= m * rji[k] / r3;         //now after this series of loops we have the initial acceleration per body
                    }
                }
            }
            }

            absv = sqrt(-a[0][0]);                          //initial velocity value
            for (int i = 0; i < N; i++){
                phi = i * 2 * pi / 3;                       //The velocity settings to commence reflect that of the starting position seen above.
                v[i][0] = - absv * sin (phi);
                v[i][1] = absv * cos (phi);                 
                v[i][2] = 0;                                //Because both r[i][2] and v[i][2] are 0, we are effectively working within a two dimensional space of x and y and then a third time dimension added on. The 3rd dimension, height, r[i][3] is 0 and thus always on the same plane, same height.
            }

            for (t = 0; t < end_t; t += dt) {               //for loops that then do the calculations every step for the bodies, outputs will include the location, velocity and acceleration of each body as arrays
                for (i = 0; i < N; i++) { 
                    rji[3];
                    for (k = 0; k < 3; k++) {
                        v[i][k] += a[i][k] * dt/2;          //velocity
                    }
                    for (k = 0; k < 3; k++) {
                        r[i][k] += v[i][k] * dt;            //location
                    }
                    for (i = 0; i < N; i++) {
                       for (k = 0; k < 3; k++) {
                        a[i][k] = 0.0;                      //acceleration
                       }
                    }
                }
                    for (i = 0; i <N; i++) {
                        for (int j = i+1; j < N; j++){
                            rji[3];
                            for (int k = 0; k < 3; k++) {
                                rji[k] = r[j][k] - r[i][k];
                            }
                            r2 = 0;
                            for (int k = 0; k < 3; k++) {
                                r2 += rji[k] * rji[k];
                            }
                            r3 = r2 * sqrt(r2);
                            for (int k = 0; k < 3; k++){
                                a[i][k] += m * rji[k] / r3;
                                a[j][k] -= m * rji[k] / r3;
                            }
                        }
                    }
                }
    return 0;
    }

