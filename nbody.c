    #include <stdio.h>
    #include <math.h>
    #include <stdlib.h>
    #include <time.h>
    #include <unistd.h>
    #include <sys/types.h>
    #include <sys/time.h>
    
    double acceleration(int i, double x, double G, double m);
    double timestep(double (*ap)(double), double x0, double v0, double m, double dt);
    double initial_cond(int N, int D);
    double simulate(int N, int D, int S, double G, int dt);
    
    int main() {
    
            x0, v0, m = initial_cond(10,2);
            x1, v1 = timestep(x0, v0, 1.0, m, 1.0e-3);
    
            double a;
    
            a = acceleration(i, x, G, m);
    
    
            Ns[] = {2, 4, 8, 16, 64, 128, 256};
            runtimes[];
            clock_t start, end;
            double cpu_time_used;
            int i;
            for (N=0, N < Ns, Ns++) {
                    start = clock();
                    call simulate(N, 3, 100, 1.9, 1e-3);
                    end = clock();
                    cpu_time_used = ((double) (end-start))/ CLOCKS_PER_SEC;
                    runtimes[i] = cpu_times_used;
            }
            print(runtimes[]);
    
            return 0;
    }
    
    /* def remove_i(x, i): routine must be written */
    
    double acceleration(int i, double x, double G, double m) {
            x_i = x[i];
            x_j = // remove_i(x,i);
            m_j = // remove_i(m,i);
            diff = x_j -x_i;
            mag3 = // np.sum(diff**2, axis=1)**1.5
            result = G * //np.sum(diff *(m_j / mag3)[;,np.newaxis], axis=0)
    
            return result;}
    
    double timestep(double (*a)(double), double x0, double v0, double m, double dt) {
            double N, ai_0;
            double x1, v1;
            N = strlen(x0);
            x1 = // np.empty(x0.shape, dtype=float);
            v1 = // np.empty(v0.shape, dtype=float);
            for (i=0, i<N, i++) {
                    a_i0 = a(i, x0, G, m);
                    v1[i] = a_i0 * dt + v0[i];
                    x1[i] = a_i0 * dt**2 + v0[i] * dt + x0[i];
            return x1, v1; //work on the return function here
            }
    
    double initial_cond(int N, int D) { //check pointers
            v0 [];
            m [];
            stand(42);
            x0[] = // (double) (1.0)*(rand() / (RAND_MAX + 1.0));
            memset(v0, 0.0, sizeof v0); //why did they apply it to N,D)
            memset(m, 1.0, sizeof m); //why they apply it to only N
            return x0, v0, m; //can only return one, the rest will have to be hybrid method from quiz4
    }
    
    double simulate(int N, int D, int S, double G, int dt) {
             x0 = initial_cond(x0, &v0, &m, N, D);
             for (i=0, i<S, i++) {
                     x1 = timestep(&v1, x0, G, m, dt);
                     x0 = x1;
                     v0 = v1;
            }
    
    
     /* Sources
     * https://www.geeksforgeeks.org/how-to-measure-time-taken-by-a-program-in-c/
     * https://stackoverflow.com/questions/5636070/zero-an-array-in-c-code
     *
