This is the changelog of the programs 2body_euler.c and 2body_leapfrog.c

# 2body_euler.c

## Initial Code.

    #include <stdio.h>
    #include <math.h>
    #include <stdlib.h>
    
    int main() {
            double r[3], v[3], a[3];                //Arrays containing the three Cartesian components, position (r), velocity (v) and acceleration (a). All of them are three dimensional
            double dt;                              //Timestep
            int i, k;
    
            dt = 1e-5;
    
            r[0] = 1;
            r[1] = 0;
            r[2] = 0;
            v[0] = 0;
            v[1] = 0.5;
            v[2] = 0;
    
            for (i = 0; i < 100000; i++){
                    double r2 = r[0]*r[0] + r[1]*r[1] + r[2]*r[2];  //square of the distance between the two bodies
                    for (int k = 0; k < 3; k++)
                            a[k] = - r[k] / (r2 * sqrt(r2));        //acceleration a[k]
                            r[k] += v[k] * dt;                      //new position after being affected by acceleration r[k]
                            v[k] += a[k] * dt;                      //new velocity
            }
            printf(" \n   r[k]=%10.8f \n", r[k]);
            printf(" \n   v[k]=%10.8f \n", v[k]);
            printf(" \n   a[k]=%10.8f \n", a[k]);
    
            return 0;
    }
    
### Output of initial code.

    $ gcc n2euler.c ; ./a
    r[k]=   0.35070261                   //They are still orbiting each other since the distance difference is less than 1
    v[k]=  -1.92426437  
    a[k]=  -8.12969663              

### Changing different variables.

Amending the number of times we integrate as following,

Instead of,

    for (i = 0; i < 100000; i++)

We have,

    for (i = 0; i < 10000; i++)

We get,

    r[k]=   0.99499214
    v[k]=  -0.10033508
    a[k]=  -1.01008942

If we have,

    for (i = 0; i < 1000000; i++)

We get,

    r[k]=-11244.98096452             //They are very far from each other.
    v[k]=-1265.01171107              //Travelling really fast
    a[k]=   0.00000001               //The absence of acceleration means that the bodies have "disconnected" are not orbiting each other anymore

This doesn't make sense since they should settle in orbit around each other. Rather they have been flung apart.

If we change the timestep from dt=1e-5 to,

    dt = 1e-7

We get,

    r[k]=0.99999950
    v[k]=-0.00100000
    a[k]=-1.00000100

And dt increased again,

    dt = 1e-10

We get,

    r[k]=1.00000000
    v[k]=-0.00000100
    a[k]=-1.00000000               //Meaning that we are nearly in perfect orbit 

## Update making a data1.txt file.

In this version of the code, the data output is placed in a data1.txt file and executed in octave to get a plot. The data output placed in the data1.txt file is composed of the 2d vector for position. r[] is an array and thus r[1] and r[2] make up x and y which can then be plotted.

    #include <stdio.h>
    #include <math.h>
    #include <stdlib.h>
    
    int main() {
            double r[3], v[3], a[3];                
            double dt;                              
            int i, k;
            FILE *fp;
    
            dt = 1e-2;
    
            r[0] = 1;
            r[1] = 0;
            r[2] = 0;
            v[0] = 0;
            v[1] = 0.5;
            v[2] = 0;
    
            fp = fopen("data1.txt", "w+");
            for (i = 0; i < 1000; i++){
                    double r2 = r[0]*r[0] + r[1]*r[1] + r[2]*r[2];  
                    for (k = 0; k < 3; k++) {
                            a[k] = - r[k] / (r2 * sqrt(r2));        
                    }
                    for (k = 0; k< 3; k++) {
                            r[k] += v[k] * dt;                      
                            v[k] += a[k] * dt;                      
                    }
            fprintf(fp, "%f %f \n", r[0], r[1]);
            }
    
            fclose(fp);
    
            printf(" \n   r[0]=%10.8f \n", r[0]);
            printf(" \n   r[1]=%10.8f \n", r[1]);
            printf(" \n   r[2]=%10.8f \n", r[2]);
            printf(" \n   v[0]=%10.8f \n", v[0]);
            printf(" \n   v[1]=%10.8f \n", v[1]);
            printf(" \n   v[2]=%10.8f \n", v[2]);
    
    return 0;
    
    }

File data1.txt is in the repository.

When run in Octave according to,

    >> data=dlmread("data1.txt");
    >> plot(data(:,1),data(:,2))
    
Output,

![2bodyfig](Images/2bodyfig1.jpg){: .shadow.small}

The star flings one of them out.

## Changing the timestep and number of iterations.

Taking dt = 1e-2 and decreasing the length of the timestep to be,

    dt = 1e-3
    
Also changing the number of loops from i < 1000 to be,

    i < 10000
    
We get a text file 2body_euler_data2.txt and the image below after compilation in octave,

    >> data2=dlmread("data2.txt");
    >> plot(data2(:,1),data2(:,2))

And the image,

![2bodyfig2](Images/2bodyfig2.jpg)

Where the stars are not in exact orbit, one of them is running away gradually.

Pushing the executable's limits on Octave for the plotting we go up to,

    dt = 1e-5

And a number of loops,

    i < 1000000

We produce text file 2body_euler_data3.txt and the image below is the compilation from octave,

![2bodyfig3](Images/2bodyfig3.jpg)

# 2body_leapfrog.c

## Initial Code.

    #include <stdio.h>
    #include <math.h>
    #include <stdlib.h>
    
    int main() {
            double r[3], v[3], a[3];
            double dt;
            int i, k;
            FILE *fp;
    
            dt = 1e-5;
    
            r[0] = 1;
            r[1] = 0;
            r[2] = 0;
            v[0] = 0;
            v[1] = 0.5;
            v[2] = 0;
    
            fp = fopen("data4.txt", "w+");
            for (i = 0; i < 1000000; i++) {
                    double r2 = r[0]*r[0] + r[1]*r[1] + r[2]*r[2];
                    for (int k = 0; k < 3; k++) {
                            a[k] = - r[k] / (r2 * sqrt(r2));
                            v[k] += 0.5 * a[k] * dt;
                            r[k] += v[k] * dt;
                    }
            fprintf(fp, "%f %f \n", r[0], r[1]);
            }
            fclose(fp);
    
            printf(" \n   r[0]=%10.8f \n", r[0]);
            printf(" \n   r[1]=%10.8f \n", r[1]);
            printf(" \n   r[2]=%10.8f \n", r[2]);
            printf(" \n   v[0]=%10.8f \n", v[0]);
            printf(" \n   v[1]=%10.8f \n", v[1]);
            printf(" \n   v[2]=%10.8f \n", v[2]);
    
    
        return 0;
    }

## Comparing the output with 2body_euler.c

Interestingly with the same input parameters,

    dt = 1e-5
    i < 1000000
    
And the sole difference being the operation. The time of operation are halved with the leapfrog. 
Time of execution for 2body_euler.c executable,

    real    0m33.109s
    user    0m30.000s
    sys     0m0.249s

Whereas for the 2body_leapfrog.c executable we're at,

    real    0m15.760s
    user    0m15.593s
    sys     0m0.108s

The accuracy does change though and decreases, the leapfrog is a second order integrator compared to a first order integrator for euler.

The output image and orbit can be seen below,

![2bodyfig4](Images/2bodyfig4.jpg)

The leapfrog algorithm is a step towards building the proper 3body algorithm.

