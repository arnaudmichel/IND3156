    #include <stdio.h>
    #include <math.h>
    #include <stdlib.h>
    
    int main() {
            double r[3], v[3], a[3];                //Arrays containing the three Cartesian components, position (r), velocity (v) and acceleration (a). All of them are three dimensional
            double dt;                              //Timestep
            int i, k;
            FILE *fp;
            
            dt = 1e-5;
    
            r[0] = 1;
            r[1] = 0;
            r[2] = 0;
            v[0] = 0;
            v[1] = 0.5;
            v[2] = 0;
            
            fp = fopen("data.txt", "w+");
            for (i = 0; i < 100000; i++){
                    double r2 = r[0]*r[0] + r[1]*r[1] + r[2]*r[2];  //square of the distance between the two bodies
                    for (k = 0; k < 3; k++) {
                            a[k] = - r[k] / (r2 * sqrt(r2));        //acceleration a[k]
                    }
                    for (k = 0; k< 3; k++) {
                            r[k] += v[k] * dt;                      //new position after being affected by acceleration r[k]
                            v[k] += a[k] * dt;                      //new velocity
                    }                                               //this loops many times to have a bunch of tangents of the orbit
            fprintf(fp, "%f %f \n", r[0], r[1]);
            }
    
            fclose(fp);
    
            printf(" \n   r[0]=%10.8f \n", r[0]);
            printf(" \n   r[1]=%10.8f \n", r[1]);
            printf(" \n   r[2]=%10.8f \n", r[2]);
            printf(" \n   v[0]=%10.8f \n", v[0]);
            printf(" \n   v[1]=%10.8f \n", v[1]);
            printf(" \n   v[2]=%10.8f \n", v[2]);
    
            return 0;
    }